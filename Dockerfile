FROM golang:alpine as go_builder

RUN apk add --no-cache musl-dev

WORKDIR /go/src/app
COPY *.go go.* ./
RUN go mod download
RUN CGO_ENABLED=false go build -o app .

FROM alpine:3.10
RUN apk add --no-cache ca-certificates
WORKDIR /
COPY --from=go_builder /go/src/app/app /app
COPY templates /templates
ENTRYPOINT ["/app"]